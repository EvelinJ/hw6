import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {
   String nl = System.getProperty ("line.separator");

   @Test (timeout=20000)
   public void testRemoveVertexWithArcs() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      GraphTask.Vertex v6 = g.createVertex("v6");
      GraphTask.Vertex v5 = g.createVertex("v5");
      GraphTask.Vertex v4 = g.createVertex("v4");
      GraphTask.Vertex v3 = g.createVertex("v3");
      GraphTask.Vertex v2 = g.createVertex("v2");
      GraphTask.Vertex v1 = g.createVertex("v1");
      g.createArc("a" + v1.toString() + "_" + v3.toString(), v1, v3);
      g.createArc("a" + v2.toString() + "_" + v5.toString(), v2, v5);
      g.createArc("a" + v2.toString() + "_" + v1.toString(), v2, v1);
      g.createArc("a" + v2.toString() + "_" + v3.toString(), v2, v3);
      g.createArc("a" + v3.toString() + "_" + v1.toString(), v3, v1);
      g.createArc("a" + v3.toString() + "_" + v6.toString(), v3, v6);
      g.createArc("a" + v3.toString() + "_" + v2.toString(), v3, v2);
      g.createArc("a" + v3.toString() + "_" + v5.toString(), v3, v5);
      g.createArc("a" + v4.toString() + "_" + v6.toString(), v4, v6);
      g.createArc("a" + v4.toString() + "_" + v3.toString(), v4, v3);
      g.createArc("a" + v4.toString() + "_" + v5.toString(), v4, v5);
      g.createArc("a" + v5.toString() + "_" + v2.toString(), v5, v2);
      g.createArc("a" + v5.toString() + "_" + v1.toString(), v5, v1);
      g.createArc("a" + v5.toString() + "_" + v4.toString(), v5, v4);
      g.createArc("a" + v5.toString() + "_" + v6.toString(), v5, v6);
      g.createArc("a" + v6.toString() + "_" + v3.toString(), v6, v3);
      g.createArc("a" + v6.toString() + "_" + v5.toString(), v6, v5);

      g.createAdjMatrix();
      g.removeVertexWithArcs(2);

      GraphTask.Graph r = new GraphTask().new Graph("G");
      GraphTask.Vertex vr6 = r.createVertex("v6");
      GraphTask.Vertex vr5 = r.createVertex("v5");
      GraphTask.Vertex vr4 = r.createVertex("v4");
      GraphTask.Vertex vr2 = r.createVertex("v2");
      GraphTask.Vertex vr1 = r.createVertex("v1");
      r.createArc("a" + vr2.toString() + "_" + vr5.toString(), vr2, vr5);
      r.createArc("a" + vr2.toString() + "_" + vr1.toString(), vr2, vr1);
      r.createArc("a" + vr4.toString() + "_" + vr6.toString(), vr4, vr6);
      r.createArc("a" + vr4.toString() + "_" + vr5.toString(), vr4, vr5);
      r.createArc("a" + vr5.toString() + "_" + vr2.toString(), vr5, vr2);
      r.createArc("a" + vr5.toString() + "_" + vr1.toString(), vr5, vr1);
      r.createArc("a" + vr5.toString() + "_" + vr4.toString(), vr5, vr4);
      r.createArc("a" + vr5.toString() + "_" + vr6.toString(), vr5, vr6);
      r.createArc("a" + vr6.toString() + "_" + vr5.toString(), vr6, vr5);

      assertEquals ("Remove vertex with arcs: ", r+nl, g+nl);
   }

   @Test (timeout=20000)
   public void testRemoveFirstVertexWithArcs() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      GraphTask.Vertex v3 = g.createVertex("v3");
      GraphTask.Vertex v2 = g.createVertex("v2");
      GraphTask.Vertex v1 = g.createVertex("v1");
      g.createArc("a" + v1.toString() + "_" + v3.toString(), v1, v3);
      g.createArc("a" + v1.toString() + "_" + v2.toString(), v1, v2);
      g.createArc("a" + v2.toString() + "_" + v1.toString(), v2, v1);
      g.createArc("a" + v2.toString() + "_" + v3.toString(), v2, v3);
      g.createArc("a" + v3.toString() + "_" + v2.toString(), v3, v2);
      g.createArc("a" + v3.toString() + "_" + v1.toString(), v3, v1);

      g.createAdjMatrix();
      g.removeVertexWithArcs(0);

      GraphTask.Graph r = new GraphTask().new Graph("G");
      GraphTask.Vertex vr3 = r.createVertex("v3");
      GraphTask.Vertex vr2 = r.createVertex("v2");
      r.createArc("a" + vr2.toString() + "_" + vr3.toString(), vr2, vr3);
      r.createArc("a" + vr3.toString() + "_" + vr2.toString(), vr3, vr2);

      assertEquals ("Remove first vertex: ", r+nl, g+nl);
   }

   @Test (timeout=20000)
   public void testRemoveLastVertexWithArcs() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      GraphTask.Vertex v3 = g.createVertex("v3");
      GraphTask.Vertex v2 = g.createVertex("v2");
      GraphTask.Vertex v1 = g.createVertex("v1");
      g.createArc("a" + v1.toString() + "_" + v3.toString(), v1, v3);
      g.createArc("a" + v1.toString() + "_" + v2.toString(), v1, v2);
      g.createArc("a" + v2.toString() + "_" + v1.toString(), v2, v1);
      g.createArc("a" + v2.toString() + "_" + v3.toString(), v2, v3);
      g.createArc("a" + v3.toString() + "_" + v2.toString(), v3, v2);
      g.createArc("a" + v3.toString() + "_" + v1.toString(), v3, v1);

      g.createAdjMatrix();
      g.removeVertexWithArcs(2);

      GraphTask.Graph r = new GraphTask().new Graph("G");
      GraphTask.Vertex vr2 = r.createVertex("v2");
      GraphTask.Vertex vr1 = r.createVertex("v1");
      r.createArc("a" + vr1.toString() + "_" + vr2.toString(), vr1, vr2);
      r.createArc("a" + vr2.toString() + "_" + vr1.toString(), vr2, vr1);

      assertEquals ("Remove last vertex: ", r+nl, g+nl);
   }

   @Test (timeout=20000)
   public void testRemoveVertexWithArcsGraphHasOneVertex() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      GraphTask.Vertex v1 = g.createVertex("v1");

      g.createAdjMatrix();
      g.removeVertexWithArcs(0);

      GraphTask.Graph r = new GraphTask().new Graph("G");

      assertEquals ("Remove vertex with arcs if graphs has one vertex: ", r+nl, g+nl);
   }



   @Test (expected=RuntimeException.class)
   public void testEmptyGraph() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      g.removeVertexWithArcs(0);
   }

   @Test (expected=RuntimeException.class)
   public void testRemoveIllegalVertex() {
      GraphTask.Graph g = new GraphTask().new Graph("G");
      g.createVertex("v3");
      g.createVertex("v2");
      g.createVertex("v1");
      g.removeVertexWithArcs(4);
   }

}

