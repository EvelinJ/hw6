import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9);
      System.out.println (g);


      int n = 2;
      System.out.println("Graafi esimene tipp asub indeksil 0. Kasutaja soovib eemaldada tippu, mis asub graafis indeksil " + n);
      g.removeVertexWithArcs(n);
      System.out.println(g);


   }

   /** Vertex represents one point in the graph.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed
      private Vertex eemaldtatavTipp = null;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         //G nimi ehk id
         sb.append (id);
         sb.append (nl);
         //G esimene tipp
         Vertex v = first;
         while (v != null) {
            //tipu nimi
            sb.append (v.toString());
            sb.append (" -->");
            //v'st väljuv eimene kaar
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               //kaare nimi, näiteks AB
               sb.append (a.toString());
               sb.append (" (");
               //tipu nimi A
               sb.append (v.toString());
               sb.append ("->");
               //tipust sihtkoha tipu nimi ehk kui on kaar AB siis target on B
               sb.append (a.target.toString());
               sb.append (")");
               //järgmine kaar, kuni next on null
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         //graafi esimene tipp
         Vertex v = first;
         //nummerdatakse tipud 0, 1, 2 ...
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         //Käime teist korda graafi tipud üle ja võtame infost indeksi i
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            //käime kõik kaared üle ja võtame targetist info j
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Koostada meetod etteantud graafist etteantud tipu eemaldamiseks
       * (eemaldada tuleb ka kõik sellesse tippu suubuvad ning sellest tipust väljuvad kaared).
       * @param n eemaldatatava tipu indeks
       */
      public void removeVertexWithArcs(int n) {

         Vertex v = first;
         int countVertex = 0;

         if (v == null) {
            throw new RuntimeException("Graafil ei ole ühtegi tippu.");
         }

         while (v != null){
            countVertex++;
            v = v.next;
         }

         if (n >= countVertex) {
            throw new RuntimeException("Soovite eemaldada tippu, mida ei ole graafis. Graafil ei ole nii palju tippe.");
         }

         v = first;
         while (v != null) {

            //Esimesel kohal oleva tipu eemaldamine
            if (v.info == n){
               eemaldtatavTipp = v;
               first = v.next;
            }

            //Teisel kuni viimasel kohal oleva tipu eemaldamine
            if (v.next!= null && v.next.info == n) {
               eemaldtatavTipp = v.next;
               //System.out.println("eemaldtatavTipp: " + eemaldtatavTipp);
               v.next = v.next.next;
            }

            v = v.next;

         }

         //Eemaldame kaared, mille sihtkoht on eemaldtatavTipp
         v= first;
         if (v != null){
            removeArcs();
         }

      }

      /**
       * Koostada meetod etteantud graafist etteantud tippu suubuvate kaarte eemaldamiseks.
       */
      public void removeArcs (){
         Vertex v = first;
         Arc a = first.first;

         while (v != null) {

            while ( a != null) {
               //Esimesel kohal oleva kaare eemaldamine
               if (a.target.equals(eemaldtatavTipp)){
                  v.first = a.next;
               }

               //Teisel kuni viimasel kohal oleva kaare eemaldamine
               if (a.next != null && a.next.target.equals(eemaldtatavTipp)){
                  //System.out.println(eemaldtatavTipp);
                  a.next = a.next.next;
               }

               a = a.next;
            }

            if (v.next != null) {
               a = v.next.first;
            }

            v = v.next;

         }
      }

   }

} 

